"""
Checkers.py
Creates a checkerboard and plays the game Checkers

Author: Richard Harrington
Date Created: 9/17/2013
Last Modified: 9/22/2013
"""

# TODO: Get images of checker pieces and import them
# TODO: Don't end turn if player puts piece down where it started
# TODO: End turn when no more jumps possible

from tkinter import *
from math import floor

class Checkerboard():
    H,W=0,0
    colors=[]
    def __init__(self,height,width,color1,color2):
        # Accepts height, width, and two color values
        self.H,self.W=height,width
        self.colors.append(color1)
        self.colors.append(color2)
        
    def px_to_tile(self,val,d):
        # Converts an input value to one of eight positions
        pos=floor(1000*val/d)
        if pos in range (0,125):
            return 0
        elif pos in range(125,250):
            return 1
        elif pos in range(250,375):
            return 2
        elif pos in range(375,500):
            return 3
        elif pos in range(500,625):
            return 4
        elif pos in range(625,750):
            return 5
        elif pos in range(750,875):
            return 6
        elif pos in range(876,1000):
            return 7

class Checkers(Checkerboard):
    myCB=Checkerboard(640,640,"black","red")
    picked_up=False
    origin=[]
    turn=1
    board=[[i for i in range(8)] for j in range(8)]
    jump=False
    
    def __init__(self):
                
        master=Tk()
        c=Canvas(master, width=self.myCB.W,height=self.myCB.H)
        c.pack()
        
        for x in range(8):
            for y in range(8):
                x_start=floor(x*self.myCB.W/8)
                y_start=floor(y*self.myCB.H/8)
                x_stop=floor((x+1)*self.myCB.W/8)
                y_stop=floor((y+1)*self.myCB.W/8)
                m=lambda x:x%2

                # Build, color and draw the board
                if (m(y)and m(x)) or (not m(y) and not m(x)):
                    color=self.colors[0]
                else:
                    color=self.colors[1]
                c.create_rectangle(x_start,y_start,x_stop,y_stop,fill=color)

                # Initialize the pieces
                if ((m(y)==0 and m(x)==1) or (m(y)==1 and m(x)==0)):
                    if x<3:
                        self.board[x][y]=self.colors[0]
                    elif x>4:
                        self.board[x][y]=self.colors[1]
                    else:
                        self.board[x][y]="open"
                else:
                    self.board[x][y]="invalid"

        master.bind("<Button-1>", self.callback)
        
        
        master.title("Checkers")
        
        master.mainloop()
        
    def callback(self,event):
        
        # Converts the X,Y pixel locations of the event to a Column,Row position
        col=self.px_to_tile(event.x,self.myCB.W)
        row=self.px_to_tile(event.y,self.myCB.H)
        clicked=[row,col]
        
        if not self.picked_up:
            if self.board[row][col] in ["red","black","king_r","king_b"]:
                if self.colors[self.turn%2][0] in self.board[row][col]:
                    self.picked_up=self.board[row][col]
                    self.origin=clicked
                else:
                    print("Wait your turn!")
            else:
                print("Please click on a square with your piece on it,", self.colors[self.turn%2])
        else:
            if self.valid_move(self.origin,clicked,self.picked_up):
                if row==7 and self.picked_up=="black":
                    self.picked_up="king_b"
                elif row==0 and self.picked_up=="red":
                    self.picked_up="king_r"
                self.move(self.picked_up,self.origin,clicked,self.jump)
                self.picked_up=False
                if not self.jump: self.turn+=1
            else:
                print("Invalid move")
            self.jump=False         
        
    def valid_move(self,origin,destination,color):
        # Determine if the chosen move is valid
        o_row,o_col=int(origin[0]),int(origin[1])
        d_row,d_col=int(destination[0]),int(destination[1])

        if o_col==d_col and o_row==d_row: return True # Picked up and put back down in same place

        if self.board[d_row][d_col]!="open": return False # Can't move to a space that isn't open
        
        r_dist=self.check_distance(o_row,d_row)
        c_dist=self.check_distance(o_col,d_col)
        
        if abs(r_dist)==2 and abs(c_dist)==2: # Jumping
            j_row=o_row+floor(r_dist/2)
            j_col=o_col+floor(c_dist/2)

            if color in ["black", "king_b"]:
                # Make sure the jumped piece is the opponent's
                if self.board[j_row][j_col] in ["red", "king_r"]:
                    self.jump=[j_row,j_col]
                    return True
            elif color in ["red", "king_r"]:
                # Make sure the jumped piece is the opponent's
                if self.board[j_row][j_col] in ["black", "king_b"]:
                    self.jump=[j_row,j_col]
                    return True
        elif r_dist==1:
            if color in ["black", "king_r", "king_b"] and abs(c_dist)==1:
                return True
        elif r_dist== -1:
            if color in ["red", "king_b", "king_r"] and abs(c_dist)==1:
                return True
        return False
    
    def check_distance(self,origin,destination):
        # Checks the source and destination column for a valid move
        if origin==0:
            if destination==1:
                return 1
            elif destination==2:
                return 2
        elif origin==7:
            if destination==6:
                return -1
            elif destination==5:
                return -2
        else: return destination-origin

    def move(self,piece,origin,destination,is_jump):
        # Move a piece from a starting position to an ending position
        self.board[origin[0]][origin[1]]="open"
        self.board[destination[0]][destination[1]]=piece
        if is_jump:
            self.board[is_jump[0]][is_jump[1]]="open"

c=Checkers()
